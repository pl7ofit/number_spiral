s=15; cur=[s//2,s//2]; prio=[[0,1],[1,0],[0,-1],[-1,0]]
spiral = [[0 for i in range(0,s)] for l in range(0,s)]
for cor in enumerate([[0,0]]+[prio[c] for c in [int(c) for c in ''.join([str(p%4)*((p+2)//2) for p in range(0,s**2//2)])]][:s**2-1],start=1):
    n=cor[0];cur=[cur[0]+cor[1][0],cur[1]+cor[1][1]];spiral[cur[0]][cur[1]]=n
for c in spiral: print(str(('{:>'+str(len(str(s**2))+1)+'}')*len(c)).format(*c))
